package fizzbuzz;

/**
 * http://codingdojo.org/cgi-bin/index.pl?KataFizzBuzz
 * 
 * @author jscherer2
 *
 */
public class FizzBuzz {

	private int numbers;

	public FizzBuzz(int i) {
		numbers = i;
	}

	public String print(int i) {
		if (i%3 != 0 && i%5 != 0) {
			return String.valueOf(i);
		}
		String str = "";		
		if (i%3 == 0) {
			str += "Fizz";
		}
		if (i%5 == 0) {
			str +=  "Buzz";
		}
		return str;
		
	}

	public String printAll() {		
		StringBuilder str = new StringBuilder("");
		for (int i = 1; i <= numbers; i++) {
			str.append(print(i) + "\n");
		}
		return str.toString();
	}

}
