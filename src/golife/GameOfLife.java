package golife;

/**
 * http://codingdojo.org/cgi-bin/index.pl?KataGameOfLife
 * 
 * You start with a two dimensional grid of cells, where each cell is either
 * alive or dead. In this version of the problem, the grid is finite, and no
 * life can exist off the edges. When calcuating the next generation of the
 * grid, follow these rules:
 * 
 * 1. Any live cell with fewer than two live neighbours dies,
 *    as if caused by underpopulation. 
 * 2. Any live cell with more than three live neighbours dies,
 *    as if by overcrowding. 
 * 3. Any live cell with two or three live neighbours
 *    lives on to the next generation. 
 * 4. Any dead cell with exactly three live neighbours becomes a live cell.
 * 
 * You should write a program that can accept an arbitrary grid of cells, and
 * will output a similar grid showing the next generation.
 * 
 * @author josef.scherer at gmail.com
 *
 */
public class GameOfLife {

	static final int DEAD_CELL = 0;
	static final int LIVE_CELL = 1;

	public static final int UNDER_POPULATION = 1;

	enum Cell {
		ALIVE, DEAD;

		public String toString() {
			return this == ALIVE ? "*" : ".";
		}
	}

	private Cell[][] grid;
	private Cell[][] nextGrid;
	private int generation;
	private String patternName;

	public GameOfLife(int rows, int cols) {
		grid = new Cell[rows][cols];
		generation = 1;
		patternName = "";
	}

	private Cell[][] deepCopy(Cell[][] grid) {
		int length = getRowCount();
		Cell[][] copy = new Cell[length][];
		for (int i = 0; i < length; i++) {
			copy[i] = grid[i].clone();
		}
		return copy;
	}

	public int getRowCount() {
		return grid.length;
	}

	public int getColumnCount() {
		return grid[0].length;
	}

	public void setGrid(Cell[][] grid, String patternName) {
		this.grid = grid;
		nextGrid = deepCopy(grid);
		this.patternName = patternName;
	}

	public void setGrid(Cell[][] blinkerIn) {
		setGrid(blinkerIn, patternName);
	}

	public int getGeneration() {
		return generation;
	}
	
	public boolean isAlive(int row, int col) {
		Cell cell = getCell(row, col);
		return cell == Cell.ALIVE ? true : false;
	}

	public boolean isDead(int row, int col) {
		return !isAlive(row, col);
	}
	
	Cell getCell(int row, int col) {
		// Torus for out of bounds indices (instead of Cell.DEAD)
		int m = getColumnCount();
		int n = getRowCount();
		row = (row%m + m)%m;		
		col = (col%n + n)%n;		
		return grid[row][col];
	}

	public int countAliveNeighbours(int row, int col) {
		int count = 0;
		for (int i = row - 1; i <= row + 1; i++) {
			for (int j = col - 1; j <= col + 1; j++) {
				if (row == i && col == j) {
					continue;
				}
				if (isAlive(i, j)) {
					count++;
				}
			}
		}
		return count;
	}

	public Cell[][] transform(int row, int col) {
		int aliveNeighbours = countAliveNeighbours(row, col);
		if (isAlive(row, col) && (aliveNeighbours < 2 || aliveNeighbours > 3)) {
			nextGrid[row][col] = Cell.DEAD;
		} else if (isDead(row, col) && aliveNeighbours == 3) {
			nextGrid[row][col] = Cell.ALIVE;
		}
		return nextGrid;
	}

	public Cell[][] nextGeneration() {
		if (generation > 1) {
			setGrid(nextGrid);			
		}
		for (int row = 0; row < getRowCount(); row++) {
			for (int col = 0; col < getColumnCount(); col++) {
				transform(row, col);
			}
		}
		generation++;
		return nextGrid;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(patternName + " " + generation + ":\n");
		for (int row = 0; row < getRowCount(); row++) {
			for (int col = 0; col < grid.length; col++) {
				sb.append(nextGrid[row][col] + " ");
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
