package minesweeper;

import java.util.Arrays;

public class MineSweeper {

	static final Object MINE = "*";

	private String[][] field;
	String[][] hints;

	public MineSweeper(String[][] field) {
		this.field = field;
		this.hints = field.clone();
		initHints();
	}

	void initHints() {
		for (int row = 0; row < getRowCount(); row++) {
			for (int col = 0; col < getColumnCount(); col++) {
				if (! get(row, col).equals(MINE)) {
					set(row, col, "0");
				}
			}
			// System.out.println(Arrays.toString(hints[i]));
		}
	}

	/**
	 * @param row
	 * @param col
	 * @return
	 */
	public String get(int row, int col) {
		return hints[row][col];
	}

	public void set(int row, int col, String value) {
		hints[row][col] = value;
	}
	
	public String[][] getHints() {
		// System.out.println(this);
		return hints;
	}

	public int getColumnCount() {
		return field[0].length;
	}

	public int getRowCount() {
		return field.length;
	}

	public void calculateHints() {
		for (int row = 0; row < getRowCount(); row++) {
			for (int col = 0; col < getColumnCount(); col++) {
				if (get(row, col).equals(MINE)) {
					incrementNeighbours(row, col);
				}
			}
		}
	}

	public int incrementHint(int row, int col) {
		int incr = 0;
		if (!validRow(row) || !validColumn(col)) {
			return incr;
		}
		String value = get(row, col);
		if (value.equals(MINE)) {
			return incr;
		}
		incr = Integer.parseInt(value) + 1;
		set(row, col, String.valueOf(incr));
		return incr;
	}

	public void incrementNeighbours(int row, int col) {
		for (int i = row - 1; i <= row + 1; i++) {
			for (int j = col - 1; j <= col + 1; j++) {
				if (i != row || j != col) {
					try {
						incrementHint(i, j);
					} catch (Exception e) {
						continue;
					}
				}
			}
		}
	}

	@Override
	public String toString() {
		String str = "";
		for (int row = 0; row < getRowCount(); row++) {
			str += Arrays.toString(hints[row]) + "\n";
		}
		return str;
	}

	private boolean validColumn(int col) {
		return col >= 0 && col < getColumnCount();
	}

	private boolean validRow(int row) {
		return row >= 0 && row < getRowCount();
	}
}