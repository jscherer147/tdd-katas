package string.calculator;

public class StringCalculator {

	public static int add(String numbers) {
		if (numbers.length() == 0) {
			return 0;
		}
		// Arrays.stream(numbers.split("[,\n]")).mapToInt(Integer::parseInt).sum(); // Java 8
		return add(numbers.split("[,\n]"));
	}

	static int add(String[] digits) {
		int sum = 0;
		for (int i = 0; i < digits.length; i++) {
			sum += Integer.parseInt(digits[i]);
		}
		return sum;
	}
}
