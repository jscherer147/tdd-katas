package sudoku;

import java.util.Set;
import java.util.TreeSet;

public class Cell {

	private int value;
	private TreeSet<Integer> options = new TreeSet<Integer>() {{
		for (int i = 1; i < 10; i++) {
			add(i);
		}
	}};

	public Cell(int value) {
		setValue(value);
	}

	public Cell() {
		this.value = 0;
	}

	public int getValue() {
		return value;
	}
	
	// TODO Invalid value not in options
	public void setValue(int value) {
		this.value = value;
		this.options = new TreeSet<Integer>();
	}

	public Set<Integer> getOptions() {
		return options;
	}

	public int optionsSize() {
		return options.size();
	}

	public void removeOption(Integer i) {
		options.remove(i);
		if (optionsSize() == 1) {
			setValue(options.last());
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Cell))
			return false;
		Cell other = (Cell) obj;
		if (value != other.value)
			return false;
		return true;
	}
}
