package sudoku;

import java.util.Arrays;

public class Square {
	
	private Cell[] cells = new Cell[9];
	
	public Square() {
		
	}

	public void setCell(int index, int value) {
		cells[index] = new Cell(value);
	}

	public int getCell(int index) {
		return cells[index].getValue();
	}

	public boolean isSolved() {
		for (int i = 0; i < cells.length; i++) {
			if (! contains(i)) {
				return false;
			};
		}
		return true;
	}

	boolean contains(int i) {
		return Arrays.asList(cells).contains(new Cell(1));
	}

}
