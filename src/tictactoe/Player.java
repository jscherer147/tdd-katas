package tictactoe;

public class Player {

	TicTacToe board;
	private int type;
	
	public Player(int type, TicTacToe board) {
		this.type = type;
		this.board = board;
	}

	public void add(String position) {
		board.add(type, position);
	}

	public boolean wins() {
		return board.wins(TicTacToe.ROW, type) || board.wins(TicTacToe.COLUMN, type);
	}

}
