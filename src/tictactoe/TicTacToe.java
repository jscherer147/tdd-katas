package tictactoe;

import java.util.Arrays;

public class TicTacToe {

	// type constants (TODO? enum)
	static final int X = 1;
	static final int O = -1;

	static final String ROW = "row";
	static final String COLUMN = "column";

	int[][] board = new int[3][3];

	public void add(int type, String position) {
		try {
			add(type, position.charAt(0) - 'A', position.charAt(1) - '1');
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private void add(int type, int col, int row) {
		board[row][col] = type;
	}

	/**
	 * protected, zero based getRow() Method, used for testing only
	 * 
	 * @param i
	 *            zero based
	 * @return
	 */
	int[] getRow(int i) {
		return board[i];
	}

	int countTypeIn(String dim, int index, int type) {
		int count = 0;
		int value = 0;

		for (int j = 0; j < board.length; j++) {
			value = dim.equals(ROW) ? board[index][j] : board[j][index];
			if (value == type) {
				count++;
			}
		}

		return count;
	}
	
	boolean wins(String dim, int type) {
		for (int i = 0; i < board.length; i++) {
			if (countTypeIn(dim, i, type) == 3) {
				return true;
			}
		}
		return false;
	}

	public boolean gameOver() {
		return wins(ROW, X) || wins(ROW, O);
	}

	@Override
	public String toString() {

		String str = "  A, B, C \n";
		for (int i = 0; i < board.length; i++) {
			str += i + Arrays.toString(board[i]) + "\n";
		}
		return str;
	}

}
