package fizzbuzz;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * http://codingdojo.org/cgi-bin/index.pl?KataFizzBuzz
 * 
 * Write a program that prints the numbers from 1 to 100. 
 * But for multiples of three print "Fizz" instead of the number and 
 * for the multiples of five print "Buzz". 
 * For numbers which are multiples of both three and five print "FizzBuzz".
 * 
 * Example String from 1 to 16:
 * 
 * 		"1\n2\nFizz\n4\nBuzz\nFizz\n7\n8\nFizz\nBuzz\n11\nFizz\n13\n14\nFizzBuzz\n16\n"
 * 
 * @author jscherer2
 * 
 */
public class FizzBuzzTest {
	
	static final String fb16 = "1\n2\nFizz\n4\nBuzz\nFizz\n7\n8\nFizz\nBuzz\n11\nFizz\n13\n14\nFizzBuzz\n16\n";	
	FizzBuzz fb;
	
	@Before
	public void setUp() throws Exception {
		fb = new FizzBuzz(100);
	}

	@Test
	public void testNumbers() {
		assertEquals("1", fb.print(1));
		assertEquals("2", fb.print(2));
		assertEquals("4", fb.print(4));
		assertEquals("14", fb.print(14));
		assertEquals("16", fb.print(16));
	}
	
	@Test
	public void testFizz() {
		assertEquals("Fizz", fb.print(3));
		assertEquals("Fizz", fb.print(6));
		assertEquals("Fizz", fb.print(9));
		assertEquals("Fizz", fb.print(12));
	}
	
	@Test
	public void testBuzz() {
		assertEquals("Buzz", fb.print(5));
		assertEquals("Buzz", fb.print(10));
	}

	@Test
	public void testFizzBuzz() {
		assertEquals("FizzBuzz", fb.print(15));
	}
	
	@Test
	public void testFizzBuzz16() throws Exception {
		fb = new FizzBuzz(16);
		assertEquals(fb16, fb.printAll());
	}
}
