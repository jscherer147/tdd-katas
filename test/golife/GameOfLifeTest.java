package golife;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import golife.GameOfLife.Cell;

public class GameOfLifeTest {
	
	static final Cell O = Cell.DEAD;
	static final Cell I = Cell.ALIVE;

	static final Cell[][] UNDER_POPULATION_IN 	= { { O, O, O }, { O, I, O }, { O, O, I } };
	static final Cell[][] UNDER_POPULATION_OUT 	= { { O, O, O }, { O, O, O }, { O, O, I } };
	static final Cell[][] OVER_POPULATION_IN 	= { { I, O, O }, { I, I, O }, { O, I, I } };
	static final Cell[][] OVER_POPULATION_OUT 	= { { I, O, O }, { I, O, O }, { O, I, I } };
	static final Cell[][] REPRODUCTION_IN 		= { { I, O, O }, { I, O, O }, { O, I, O } };
	static final Cell[][] REPRODUCTION_OUT 		= { { I, O, O }, { I, I, O }, { O, I, O } };
	static final Cell[][] SURVIVAL2 = { { O, O, O }, { O, I, O }, { O, I, I } };
	static final Cell[][] SURVIVAL3 = { { O, O, O }, { I, I, O }, { O, I, I } };

	static final Cell[][] BLOCK = { 
			{ O, O, O, O }, 
			{ O, I, I, O }, 
			{ O, I, I, O }, 
			{ O, O, O, O } };
	
	static final Cell[][] BLINKER_IN = {
			{ O, O, O, O, O }, 
			{ O, O, I, O, O }, 
			{ O, O, I, O, O }, 
			{ O, O, I, O, O }, 
			{ O, O, O, O, O }};
	
	static final Cell[][] BLINKER_LEFT = {
			{ O, O, O, O, O }, 
			{ O, O, I, O, O }, 
			{ O, I, I, O, O }, 
			{ O, O, I, O, O }, 
			{ O, O, O, O, O }};

	static final Cell[][] BLINKER_RIGHT = {
			{ O, O, O, O, O }, 
			{ O, O, I, O, O }, 
			{ O, I, I, I, O }, 
			{ O, O, I, O, O }, 
			{ O, O, O, O, O }};
			
	static final Cell[][] BLINKER_OUT = {
			{ O, O, O, O, O }, 
			{ O, O, O, O, O }, 
			{ O, I, I, I, O }, 
			{ O, O, O, O, O }, 
			{ O, O, O, O, O }};
	
	static final Cell[][] GLIDER_1 = {
			{ O, I, O, O, O }, 
			{ O, O, I, O, O }, 
			{ I, I, I, O, O }, 
			{ O, O, O, O, O }, 
			{ O, O, O, O, O }};
	
	static final Cell[][] GLIDER_9 = {
			{ O, O, O, O, O }, 
			{ O, O, O, O, O },
			{ O, O, O, I, O }, 
			{ O, O, O, O, I }, 
			{ O, O, I, I, I }};
	
	static final Cell[][] LWSS_1 = {
			{ O, O, O, O, O, O, O, O }, 
			{ O, O, O, O, O, O, O, O },
			{ O, I, I, I, I, O, O, O }, 
			{ I, O, O, O, I, O, O, O }, 
			{ O, O, O, O, I, O, O, O },
			{ I, O, O, I, O, O, O, O }, 
			{ O, O, O, O, O, O, O, O },
			{ O, O, O, O, O, O, O, O }};
	
	static final Cell[][] LWSS_5 = {
			{ O, O, O, O, O, O, O, O }, 
			{ O, O, O, O, O, O, O, O },
			{ O, O, O, I, I, I, I, O }, 
			{ O, O, I, O, O, O, I, O }, 
			{ O, O, O, O, O, O, I, O },
			{ O, O, I, O, O, I, O, O }, 
			{ O, O, O, O, O, O, O, O },
			{ O, O, O, O, O, O, O, O }};
	
	GameOfLife gol;

	@Before
	public void setUp() throws Exception {
		gol = new GameOfLife(3, 3);
	}

	@Test
	public final void testCreate() {
		assertNotNull(gol);
		assertEquals(3, gol.getRowCount());
		assertEquals(3, gol.getColumnCount());
	}

	@Test
	public void testSeed() {
		gol.setGrid(BLOCK);
		assertEquals(4, gol.getRowCount());
		assertEquals(4, gol.getColumnCount());
	}

	@Test
	public void testCellIsDead() {
		assertTrue(gol.isDead(1, 1));
		assertTrue(gol.isDead(2, 2));
	}

	@Test
	public void testCountAliveNeighbours() {
		gol.setGrid(UNDER_POPULATION_IN, "Under-population");
		assertEquals(1, gol.countAliveNeighbours(1, 1));
	}

	@Test
	public void testCountAliveNeighboursBeyondBounds() {
		assertEquals(0, gol.countAliveNeighbours(0, 0));
		assertEquals(0, gol.countAliveNeighbours(2, 2));
		assertEquals(0, gol.countAliveNeighbours(0, 2));
		assertEquals(0, gol.countAliveNeighbours(2, 0));

		gol.setGrid(UNDER_POPULATION_IN, "Under-population");
//		assertEquals(1, gol.countAliveNeighbours(0, 0));
		assertEquals(1, gol.countAliveNeighbours(2, 2));
//		assertEquals(1, gol.countAliveNeighbours(0, 2));
//		assertEquals(1, gol.countAliveNeighbours(2, 0));
		assertEquals(2, gol.countAliveNeighbours(1, 2));
		assertEquals(2, gol.countAliveNeighbours(2, 1));
	}

	@Test
	public void testOverPopulation() {
		gol.setGrid(OVER_POPULATION_IN, "Over-population");
		assertEquals(4, gol.countAliveNeighbours(1, 1));
		assertArrayEquals(OVER_POPULATION_OUT, gol.transform(1, 1));
	}

	@Test
	public void testUnderPopulation() {
		gol.setGrid(UNDER_POPULATION_IN, "Under-Population");
		assertArrayEquals(UNDER_POPULATION_OUT, gol.transform(1, 1));
	}

	@Test
	public void testReproduction() {
		gol.setGrid(REPRODUCTION_IN);
		assertArrayEquals(REPRODUCTION_OUT, gol.transform(1, 1));
	}
	
	@Test
	public void testReproductionBlinkerLeftAndRight() {
		gol.setGrid(BLINKER_IN);
		assertArrayEquals(BLINKER_LEFT, gol.transform(2, 1));
		assertArrayEquals(BLINKER_RIGHT, gol.transform(2, 3));
	}
	
	@Test
	public void testSurvival2Neighbours() {
		gol.setGrid(SURVIVAL2, "Survival2");
		assertArrayEquals(SURVIVAL2, gol.transform(1, 1));
	}
	
	@Test
	public void testSurvival3Neighbours() {
		gol.setGrid(SURVIVAL3, "Survival3");
		assertArrayEquals(SURVIVAL3, gol.transform(1, 1));
	}
	
	@Test
	public void testBlockNextGeneration() {
		gol.setGrid(BLOCK, "Block");
		assertArrayEquals(BLOCK, gol.nextGeneration());
	}
	
	@Test
	public void testTransformBlinker() {
		gol.setGrid(BLINKER_IN);
		gol.transform(1, 2);
		gol.transform(2, 1);
		gol.transform(2, 3);
		assertArrayEquals(BLINKER_OUT, gol.transform(3, 2));
	}

	@Test
	public void testBlinkerNextGeneration() {
		gol.setGrid(BLINKER_IN, "Blinker");
		
		assertEquals(1, gol.getGeneration());		
		assertArrayEquals(BLINKER_OUT, gol.nextGeneration());
	}

	@Test
	public void testBlinker() {
		gol.setGrid(BLINKER_IN, "Blinker");
		gol.nextGeneration();
		assertArrayEquals(BLINKER_IN, gol.nextGeneration());
	}
	
	@Test
	public void testGlider() {
		gol.setGrid(GLIDER_1, "Glider");
		System.out.println(gol);
		for (int i = 0; i < 19; i++) {
			gol.nextGeneration();
			System.out.println(gol);
		}
		assertArrayEquals(GLIDER_1, gol.nextGeneration());
		System.out.println(gol);
	}
	
	@Test
	public void testLWSS() throws Exception {
		gol.setGrid(LWSS_1, "Light-Weight Spaceship");
//		System.out.println(gol);
		for (int i = 0; i < 15; i++) {
			gol.nextGeneration();
//			System.out.println(gol);
		}
		assertArrayEquals(LWSS_1, gol.nextGeneration());
//		System.out.println(gol);
	}
	
	// Torus: alles, was das Spielfeld nach rechts verl�sst, tritt links wieder ein.
	@Test @Ignore
	public void testGetCellColumnOutOfBounds() throws Exception {
		// TODO
	}
	
	// Torus: alles, was das Spielfeld nach unten verl�sst, tritt oben wieder ein.
	@Test @Ignore
	public void testGetCellRowOutOfBounds() throws Exception {
		// TODO
	}
}
