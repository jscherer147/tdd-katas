package minesweeper;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MineSweeperTest {


	String[][] field1 = { 
			{ "*", ".", ".", "." }, 
			{ ".", ".", ".", "." }, 
			{ ".", "*", ".", "." },
			{ ".", ".", ".", "." } };

	String[][] hints0 = { 
			{ "*", "0", "0", "0" }, 
			{ "0", "0", "0", "0" }, 
			{ "0", "*", "0", "0" },
			{ "0", "0", "0", "0" } };

	String[][] hints1 = { 
			{ "*", "1", "0", "0" }, 
			{ "1", "1", "1", "0" }, 
			{ "1", "*", "1", "0" },
			{ "1", "1", "1", "0" } };

	String[][] hintsField00 = { 
			{ "*", "1", "0", "0" }, 
			{ "1", "1", "0", "0" }, 
			{ "0", "*", "0", "0" },
			{ "0", "0", "0", "0" } };
	
	String[][] hintsField21 = { 
			{ "*", "0", "0", "0" }, 
			{ "1", "1", "1", "0" }, 
			{ "1", "*", "1", "0" },
			{ "1", "1", "1", "0" } };
	
	String[][] hints = { 
			{ "*", "1", "0", "0" }, 
			{ "2", "2", "1", "0" }, 
			{ "1", "*", "1", "0" },
			{ "1", "1", "1", "0" } };

	MineSweeper ms;

	@Before
	public void setUp() throws Exception {
		ms = new MineSweeper(field1); 
	}

	@Test
	public final void testCreate() {
		assertEquals(4, ms.getRowCount());
		assertEquals(4, ms.getColumnCount());
	}

	@Test
	public void testInitializeHints() {
		ms.initHints();
		assertArrayEquals(hints0, ms.getHints());
	}

	@Test
	public void testIncrementHint() {
		assertEquals(1, ms.incrementHint(0, 1));
		assertEquals(1, ms.incrementHint(1, 0));
		assertEquals(1, ms.incrementHint(1, 1));
		assertEquals(1, ms.incrementHint(1, 2));
		assertEquals(1, ms.incrementHint(2, 0));
		assertEquals(1, ms.incrementHint(2, 2));
		assertEquals(1, ms.incrementHint(3, 0));
		assertEquals(1, ms.incrementHint(3, 1));
		assertEquals(1, ms.incrementHint(3, 2));
		assertArrayEquals(hints1, ms.getHints());
	}

	@Test
	public void testIncrementHintTwice() {
		assertEquals(1, ms.incrementHint(1, 0));
		assertEquals(2, ms.incrementHint(1, 0));
	}

	@Test
	public void testIncrementNeighbours21() {
		ms.incrementNeighbours(2, 1);
		assertArrayEquals(hintsField21, ms.getHints());
	}

	@Test
	public void testIncrementNeighbours00() {
		ms.incrementNeighbours(0, 0);
		assertArrayEquals(hintsField00, ms.getHints());
	}
	
	@Test
	public void testIncrementNeighbours00N21() {
		ms.incrementNeighbours(0, 0);
		ms.incrementNeighbours(2, 1);
		assertArrayEquals(hints, ms.getHints());
	}
	
	@Test
	public void testCalculateHints() {
		ms.calculateHints();
		assertArrayEquals(hints, ms.getHints());
	}
}
