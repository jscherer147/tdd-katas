package string.calculator;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * String Calculator 
 * 1. Create a simple String calculator with a method int Add(string numbers) 
 * 		1. The method can take 0, 1 or 2 numbers, and will return their sum 
 * 		(for an empty string it will return 0) 
 * 		for example �� or �1� or �1,2� 
 * 		2. Start with the simplest test case of an empty string and move to 1 and two numbers 
 * 		3. Remember to solve things as simply as possible so that you force yourself
 * 		to write tests you did not think about 
 * 		4. Remember to refactor after each passing test 
 * 2. Allow the Add method to handle an unknown amount of numbers (split)
 * 3. Allow the Add method to handle new lines between numbers (instead of commas). 
 * 		1. the following input is ok: �1\n2,3� (will equal 6) 
 * 		2. the following input is NOT ok: �1,\n� (not need to prove it just clarifying)
 */
public class StringCalculatorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public final void testAddEmptyString() {
		assertEquals(0, StringCalculator.add(""));
	}

	@Test
	public final void testAddOneNumber() {
		assertEquals(1, StringCalculator.add("1"));
	}

	@Test
	public final void testAddTwoNumbers() {
		assertEquals(3, StringCalculator.add("1,2"));
	}

	@Test
	public final void testAddManyNumbers() {
		assertEquals(6, StringCalculator.add("1,2,3"));
		assertEquals(10, StringCalculator.add("1,2,3,4"));
	}

	@Test
	public final void testAddManyNumbersWithNlSeperator() {
		assertEquals(6, StringCalculator.add("1\n2,3"));
	}
	
	// TODO NOT ok �1,\n�
	@Test
	public final void testAddManyNumbersWithIllegalSeperators() {
		assertEquals(1, StringCalculator.add("1,\n"));
	}
	
	

}
