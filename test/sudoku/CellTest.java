package sudoku;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.*;

import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

public class CellTest {
	
	static final Set<Integer> FULL_RANGE = new TreeSet<Integer>() {{
		for (int i = 1; i < 10; i++) {
			add(i);
		}
	}};
	
	static final Set<Integer> EMPTY_RANGE = new TreeSet<Integer>();
	
	Cell cell;

	@Before
	public void setUp() throws Exception {
		cell = new Cell();		
	}
	
	@Test
	public void testCreateEmptyCell() {
		assertThat(cell, notNullValue());
		assertThat(cell.getValue(), is(0));
		assertThat(cell.getOptions(), is(FULL_RANGE));
		assertThat(cell.optionsSize(), is(9));
	}

	@Test
	public final void testCreateCellOne() {
		cell = new Cell(1);
		assertThat(cell, notNullValue());
		assertThat(cell.getValue(), is(1));
		assertThat(cell.getOptions(), is(EMPTY_RANGE));
		assertThat(cell.optionsSize(), is(0));
	}
	
	@Test
	public void testRemoveOptions() {
		// remove options 1-8 -> value = 9, range.size() = 0;
		for (int i = 1, max = 8; i <= 7; i++, max--) {
			cell.removeOption(i);
			assertThat(cell.optionsSize(), is(max));
		}
		cell.removeOption(8);
		assertThat(cell.getValue(), is(9));
		assertThat(cell.optionsSize(), is(0));		
	}

}
