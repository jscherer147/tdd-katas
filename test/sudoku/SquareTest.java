package sudoku;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SquareTest {

	Square sqr;
	
	@Mock
	Cell cell;

	@Before
	public void setUp() throws Exception {
		sqr = new Square();
	}

	@Test
	public void testCreateSquare() {
		assertThat(sqr, notNullValue());
	}

	@Test
	public void testGetCellValue() {
		for (int i = 0; i < 9; i++) {
			int value = i + 1;
			sqr.setCell(i, value);
			assertThat(sqr.getCell(i), is(value));
		}
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void testSetCell9IndexOutOfBoundsException() {
		sqr.setCell(9, 2);
	}

	@Test
	public void testIsSolved() throws Exception {
		// fill square
		for (int i = 0; i < 9; i++) {
			int value = i + 1;
			sqr.setCell(i, value);
		}
		assertThat(sqr.isSolved(), is(true));
	}

	@Test
	public void testSetCellRemovesOptions() throws Exception {

	}

}
