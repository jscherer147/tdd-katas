package tictactoe;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static tictactoe.TicTacToe.O;
import static tictactoe.TicTacToe.X;

import org.junit.Before;
import org.junit.Test;

public class TicTacToeTest {

	private static final int _ = 0;
	private static final int[] EMPTY_ROW = new int[] {_, _, _};
	private static final int[] WIN_X_ROW = new int[] {X, X, X};
	private static final int[] WIN_O_ROW = new int[] {O, O, O};

	private TicTacToe board;
	private Player x;
	private Player o;

	@Before
	public void setUp() throws Exception {
		board = new TicTacToe();
		x = new Player(X, board);
		o = new Player(O, board);
	}
	
	@Test
	public void testCreateBoard() {
		assertArrayEquals(EMPTY_ROW, board.getRow(0));
		assertArrayEquals(EMPTY_ROW, board.getRow(1));
		assertArrayEquals(EMPTY_ROW, board.getRow(2));
	}
	
	@Test
	public void testAddX() {
		x.add("A1");
		assertArrayEquals(new int[] {X, _, _}, board.getRow(0));
		x.add("B1");
		assertArrayEquals(new int[] {X, X, _}, board.getRow(0));
		x.add("C1");
		assertArrayEquals(WIN_X_ROW, board.getRow(0));
	}
	
	@Test
	public void testAddO() {
		o.add("A2");
		assertArrayEquals(new int[] {O, _, _}, board.getRow(1));
		o.add("C3");
		assertArrayEquals(new int[] {_, _, O}, board.getRow(2));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddIllegalArgumentA0() {
		x.add("A0");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddIllegalArgumentA4() {
		x.add("A4");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddIllegalArgumentD1() {
		x.add("D1");
	}
	
	@Test
	public void testCountTypeInRow0() throws Exception {
		x.add("A1");
		o.add("B1");
		x.add("C1");
		o.add("B2");
		x.add("B3");
		assertEquals(2, board.countTypeIn(TicTacToe.ROW, 0, X));
		assertEquals(1, board.countTypeIn(TicTacToe.ROW, 0, O));
		assertEquals(0, board.countTypeIn(TicTacToe.ROW, 1, X));
		assertEquals(1, board.countTypeIn(TicTacToe.ROW, 1, O));
		assertEquals(1, board.countTypeIn(TicTacToe.ROW, 2, X));
		assertEquals(0, board.countTypeIn(TicTacToe.ROW, 2, O));
	}
	
	@Test
	public void testCountTypeInColumn1() throws Exception {
		x.add("A1");
		o.add("B1");
		x.add("C1");
		o.add("B2");
		x.add("B3");
		assertEquals(1, board.countTypeIn(TicTacToe.COLUMN, 0, X));
		assertEquals(0, board.countTypeIn(TicTacToe.COLUMN, 0, O));
		assertEquals(1, board.countTypeIn(TicTacToe.COLUMN, 1, X));
		assertEquals(2, board.countTypeIn(TicTacToe.COLUMN, 1, O));
		assertEquals(1, board.countTypeIn(TicTacToe.COLUMN, 2, X));
		assertEquals(0, board.countTypeIn(TicTacToe.COLUMN, 2, O));
	}
	
	@Test
	public void test3xInRow0Wins() {
		add3xInRow0();
		assertArrayEquals(WIN_X_ROW, board.getRow(0));
		assertTrue(x.wins());
	}

	@Test
	public void test3oInRow1Wins() {
		add3oInRow1();
		assertArrayEquals(WIN_O_ROW, board.getRow(1));
		assertTrue(o.wins());
	}

	@Test
	public void test3xInColumnAWins() {
		add3xInColA();
		assertTrue(x.wins());
	}

	@Test
	public void test3oInColumnBWins() {
		add3oInColB();
		assertTrue(o.wins());
	}

	@Test
	public void testGameOverWhen3xInRow() {
		add3xInRow0();
		assertTrue(board.gameOver());
	}
	
	@Test
	public void testGameOverWhen3oInRow() {
		add3oInRow1();
		assertTrue(board.gameOver());
	}
	
	private void add3xInRow0() {
		x.add("A1");
		x.add("B1");
		x.add("C1");
	}
	
	private void add3oInRow1() {
		o.add("A2");
		o.add("B2");
		o.add("C2");
	}
	
	private void add3xInColA() {
		x.add("A1");
		x.add("A2");
		x.add("A3");
	}
	
	private void add3oInColB() {
		o.add("B1");
		o.add("B2");
		o.add("B3");
	}
	
}
